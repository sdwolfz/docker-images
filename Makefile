# ------------------------------------------------------------------------------
# Settings
# ------------------------------------------------------------------------------

# By default the `full` goal is executed.
.DEFAULT_GOAL := full

# Docker Hub user needed for pushing images.
LOGIN := sdwolfz

# Extra arguments for `docker build`
BUILD_ARGS :=

# Ruby version used for scripting.
RUBY_VERSION := 3.1.2

# ------------------------------------------------------------------------------
# Helpers
# ------------------------------------------------------------------------------

DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(PWD)":/here -w /here

# ------------------------------------------------------------------------------
# Utility goals
# ------------------------------------------------------------------------------

# # help
#
# Displays a help message containing usage instructions and a list of available
# goals.
#
# Usage:
#
# To display the help message run:
# ```bash
# make help
# ```
.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo '  make full'
	@echo ''
	@echo 'Utility goals:'
	@echo '  - help:   Displays this help message.'
	@echo ''
	@echo 'Project goals:'
	@echo '  - full:     Builds and pushes all images.'
	@echo '  - build:    Builds all images.'
	@echo '  - push:     Pushes all images.'
	@echo '  - generate: Updates this Makefile.'
	@echo '  - ci:       Runs the project linters.'

# ------------------------------------------------------------------------------
# Project goals
# ------------------------------------------------------------------------------

.PHONY: shell/generate
shell/generate:
	@$(DOCKER_HERE) ruby:$(RUBY_VERSION)-alpine ruby scripts/generate.rb

.PHONY: generate
generate:
	@ruby scripts/generate.rb

# ------------------------------------------------------------------------------
# CI
# ------------------------------------------------------------------------------

.PHONY: ci
ci: ci/diff ci/editorconfig ci/hadolint ci/yamllint

.PHONY: ci/diff
ci/diff:
	@$(DOCKER_HERE) sdwolfz/git:latest make lint/diff

.PHONY: ci/editorconfig
ci/editorconfig:
	@$(DOCKER_HERE) sdwolfz/editorconfig-checker:latest make lint/editorconfig

.PHONY: ci/hadolint
ci/hadolint:
	@$(DOCKER_HERE) sdwolfz/hadolint:latest make lint/hadolint

.PHONY: ci/yamllint
ci/yamllint:
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

# ------------------------------------------------------------------------------
# Lint

.PHONY: lint/diff
lint/diff:
	@git diff --exit-code

.PHONY: lint/editorconfig
lint/editorconfig:
	@editorconfig-checker .

.PHONY: lint/hadolint
lint/hadolint:
	@find . -name Dockerfile | xargs hadolint

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .

# ------------------------------------------------------------------------------
# Default
# ------------------------------------------------------------------------------

.PHONY: full
full: shell/generate build push

.PHONY: full/ansible
full/ansible: build/ansible push/ansible
.PHONY: full/ansible-lint
full/ansible-lint: build/ansible-lint push/ansible-lint
.PHONY: full/aws-cli
full/aws-cli: build/aws-cli push/aws-cli
.PHONY: full/chromium
full/chromium: build/chromium push/chromium
.PHONY: full/csso-cli
full/csso-cli: build/csso-cli push/csso-cli
.PHONY: full/editorconfig-checker
full/editorconfig-checker: build/editorconfig-checker push/editorconfig-checker
.PHONY: full/emacs
full/emacs: build/emacs push/emacs
.PHONY: full/firefox
full/firefox: build/firefox push/firefox
.PHONY: full/git
full/git: build/git push/git
.PHONY: full/hadolint
full/hadolint: build/hadolint push/hadolint
.PHONY: full/hugo
full/hugo: build/hugo push/hugo
.PHONY: full/jinja2-cli
full/jinja2-cli: build/jinja2-cli push/jinja2-cli
.PHONY: full/sass
full/sass: build/sass push/sass
.PHONY: full/shellcheck
full/shellcheck: build/shellcheck push/shellcheck
.PHONY: full/terraform
full/terraform: build/terraform push/terraform
.PHONY: full/yamllint
full/yamllint: build/yamllint push/yamllint

# ------------------------------------------------------------------------------
# Build
# ------------------------------------------------------------------------------

.PHONY: build
build: \
	build/ansible \
	build/ansible-lint \
	build/aws-cli \
	build/chromium \
	build/csso-cli \
	build/editorconfig-checker \
	build/emacs \
	build/firefox \
	build/git \
	build/hadolint \
	build/hugo \
	build/jinja2-cli \
	build/sass \
	build/shellcheck \
	build/terraform \
	build/yamllint \

.PHONY: build/ansible
build/ansible:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/ansible:11.1.0-r0 -t $(LOGIN)/ansible:latest ./images/ansible
.PHONY: build/ansible-lint
build/ansible-lint:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/ansible-lint:latest ./images/ansible-lint
.PHONY: build/aws-cli
build/aws-cli:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/aws-cli:latest ./images/aws-cli
.PHONY: build/chromium
build/chromium:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/chromium:latest ./images/chromium
.PHONY: build/csso-cli
build/csso-cli:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/csso-cli:latest ./images/csso-cli
.PHONY: build/editorconfig-checker
build/editorconfig-checker:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/editorconfig-checker:latest ./images/editorconfig-checker
.PHONY: build/emacs
build/emacs:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/emacs:latest ./images/emacs
.PHONY: build/firefox
build/firefox:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/firefox:latest ./images/firefox
.PHONY: build/git
build/git:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/git:latest ./images/git
.PHONY: build/hadolint
build/hadolint:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/hadolint:latest ./images/hadolint
.PHONY: build/hugo
build/hugo:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/hugo:0.145.0 -t $(LOGIN)/hugo:latest ./images/hugo
.PHONY: build/jinja2-cli
build/jinja2-cli:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/jinja2-cli:latest ./images/jinja2-cli
.PHONY: build/sass
build/sass:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/sass:latest ./images/sass
.PHONY: build/shellcheck
build/shellcheck:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/shellcheck:latest ./images/shellcheck
.PHONY: build/terraform
build/terraform:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/terraform:1.11.0 -t $(LOGIN)/terraform:latest ./images/terraform
.PHONY: build/yamllint
build/yamllint:
	@docker build $(BUILD_ARGS) --pull -t $(LOGIN)/yamllint:latest ./images/yamllint

# ------------------------------------------------------------------------------
# Push
# ------------------------------------------------------------------------------

.PHONY: push
push: \
	push/ansible \
	push/ansible-lint \
	push/aws-cli \
	push/chromium \
	push/csso-cli \
	push/editorconfig-checker \
	push/emacs \
	push/firefox \
	push/git \
	push/hadolint \
	push/hugo \
	push/jinja2-cli \
	push/sass \
	push/shellcheck \
	push/terraform \
	push/yamllint \

.PHONY: push/ansible
push/ansible:
	@docker push $(LOGIN)/ansible:11.1.0-r0
	@docker push $(LOGIN)/ansible:latest
.PHONY: push/ansible-lint
push/ansible-lint:
	@docker push $(LOGIN)/ansible-lint:latest
.PHONY: push/aws-cli
push/aws-cli:
	@docker push $(LOGIN)/aws-cli:latest
.PHONY: push/chromium
push/chromium:
	@docker push $(LOGIN)/chromium:latest
.PHONY: push/csso-cli
push/csso-cli:
	@docker push $(LOGIN)/csso-cli:latest
.PHONY: push/editorconfig-checker
push/editorconfig-checker:
	@docker push $(LOGIN)/editorconfig-checker:latest
.PHONY: push/emacs
push/emacs:
	@docker push $(LOGIN)/emacs:latest
.PHONY: push/firefox
push/firefox:
	@docker push $(LOGIN)/firefox:latest
.PHONY: push/git
push/git:
	@docker push $(LOGIN)/git:latest
.PHONY: push/hadolint
push/hadolint:
	@docker push $(LOGIN)/hadolint:latest
.PHONY: push/hugo
push/hugo:
	@docker push $(LOGIN)/hugo:0.145.0
	@docker push $(LOGIN)/hugo:latest
.PHONY: push/jinja2-cli
push/jinja2-cli:
	@docker push $(LOGIN)/jinja2-cli:latest
.PHONY: push/sass
push/sass:
	@docker push $(LOGIN)/sass:latest
.PHONY: push/shellcheck
push/shellcheck:
	@docker push $(LOGIN)/shellcheck:latest
.PHONY: push/terraform
push/terraform:
	@docker push $(LOGIN)/terraform:1.11.0
	@docker push $(LOGIN)/terraform:latest
.PHONY: push/yamllint
push/yamllint:
	@docker push $(LOGIN)/yamllint:latest
