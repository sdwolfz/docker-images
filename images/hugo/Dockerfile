# ------------------------------------------------------------------------------
# Base

FROM golang:1.24.0-alpine

SHELL ["/bin/ash", "-c"]

# ------------------------------------------------------------------------------
# Working directory

RUN set -exo pipefail                   && \
                                           \
    echo 'Create the working directory' && \
    mkdir /work

WORKDIR /work

# ------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail                 && \
                                         \
    echo 'Installing common packages' && \
    apk add --update --no-cache \
      build-base                \
      ca-certificates           \
      curl                      \
      git                       \
      libc6-compat              \
      libstdc++                 \
      make                      \
      rsync

# ------------------------------------------------------------------------------
# Hugo

RUN set -exo pipefail                                                    && \
                                                                            \
    echo 'Seting up temporary environment variables'                     && \
    VERSION=0.145.0                                                      && \
    SHA=f6cfcfa4575ff25a08e68b638367df60b28e28a7917471c5deec6396eae26ae2 && \
    PACKAGE=hugo-"$VERSION".tar.gz                                       && \
    BASE=https://codeload.github.com/gohugoio/hugo/tar.gz/refs/tags      && \
    URL="$BASE/v$VERSION"                                                && \
                                                                            \
    echo 'Downloading Hugo'                                              && \
    curl -fsSLo "$PACKAGE" "$URL"                                        && \
                                                                            \
    echo 'Checking package signature'                                    && \
    sha256sum "$PACKAGE"                                                 && \
    echo "$SHA  $PACKAGE" | sha256sum -c -                               && \
                                                                            \
    echo 'Extracting package'                                            && \
    mkdir -p /var/hugo                                                   && \
    tar -xzf "$PACKAGE" -C /var/hugo --strip-components=1                && \
    rm -rf "$PACKAGE"                                                    && \
                                                                            \
    echo 'Compile Hugo'                                                  && \
    cd /var/hugo                                                         && \
    CGO_ENABLED=1                                                           \
    GOPATH=/var/hugo/gopath                                                 \
    go build -v -o bin/hugo --tags extended -gcflags="-e"                && \
    mv bin/hugo /usr/local/bin/                                          && \
    rm -rf /var/hugo ~/.cache ~/go

# ------------------------------------------------------------------------------
# Execution

CMD ["/usr/local/bin/hugo"]
