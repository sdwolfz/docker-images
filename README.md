# Docker Images

| Name                                             | Image                                                                           |
|--------------------------------------------------|---------------------------------------------------------------------------------|
| [Editorconfig Checker](https://github.com/editorconfig-checker/editorconfig-checker) | [sdwolfz/editorconfig-checker](https://hub.docker.com/r/sdwolfz/editorconfig-checker) |
| [Firefox](https://www.mozilla.org) | [sdwolfz/firefox](https://hub.docker.com/r/sdwolfz/firefox) |
| [Hadolint](https://github.com/hadolint/hadolint) | [sdwolfz/hadolint](https://hub.docker.com/r/sdwolfz/hadolint) |
| [Hugo](https://gohugo.io) | [sdwolfz/hugo](https://hub.docker.com/r/sdwolfz/hugo) |
| [SASS](https://sass-lang.com/) | [sdwolfz/sass](https://hub.docker.com/r/sdwolfz/sass) |
| [Ansible](https://docs.ansible.com) | [sdwolfz/ansible](https://hub.docker.com/r/sdwolfz/ansible) |
| [Ansible Lint](https://docs.ansible.com/ansible-lint) | [sdwolfz/ansible-lint](https://hub.docker.com/r/sdwolfz/ansible-lint) |
| [AWS CLI](https://aws.amazon.com/cli/) | [sdwolfz/aws-cli](https://hub.docker.com/r/sdwolfz/aws-cli) |
| [Chromium](https://www.chromium.org) | [sdwolfz/chromium](https://hub.docker.com/r/sdwolfz/chromium) |
| [CSSO Cli](https://github.com/css/csso-cli) | [sdwolfz/csso-cli](https://hub.docker.com/r/sdwolfz/csso-cli) |
| [Emacs](https://www.gnu.org/software/emacs) | [sdwolfz/emacs](https://hub.docker.com/r/sdwolfz/emacs) |
| [Git](https://git-scm.com/) | [sdwolfz/git](https://hub.docker.com/r/sdwolfz/git) |
| [Jinja2 CLI](https://github.com/mattrobenolt/jinja2-cli) | [sdwolfz/jinja2-cli](https://hub.docker.com/r/sdwolfz/jinja2-cli) |
| [Shellcheck](https://www.shellcheck.net) | [sdwolfz/shellcheck](https://hub.docker.com/r/sdwolfz/shellcheck) |
| [Terraform](https://www.terraform.io/) | [sdwolfz/terraform](https://hub.docker.com/r/sdwolfz/terraform) |
| [Yamllint](https://github.com/adrienverge/yamllint) | [sdwolfz/yamllint](https://hub.docker.com/r/sdwolfz/yamllint) |

## Requirements

| Name     | Link                               |
|----------|------------------------------------|
| `bash`   | https://www.gnu.org/software/bash  |
| `docker` | https://docs.docker.com/get-docker |
| `make`   | https://www.gnu.org/software/make  |
| `ruby`   | https://www.ruby-lang.org/         |

## Update

Run the `generate` script to update the `Makefile` and `Dockerfile` versions:

```sh
make generate
```

## Run

1. Login to docker hub (https://hub.docker.com):

```sh
docker login
```

2. Build and push:

```sh
make
```

3. Add extra build args:

```sh
make BUILD_ARGS=--no-cache
```

## Lint

Run all linters using docker images:

```sh
make ci
```

Individual linters without docker:

```sh
make lint/editorconfig
make lint/hadolint
make lint/yamllint
```

## License

The rest of the code is covered by the BSD 3-clause License, see
[LICENSE](LICENSE) for more details.
