# frozen_string_literal: true

require 'erb'

class MakefileGenerator
  def initialize(values)
    @values = values

    @template = File.read('templates/Makefile.erb')
    @linters  = ['editorconfig', 'hadolint', 'yamllint'].freeze
  end

  def call
    images = Dir.glob('images/*').sort

    @images = images.map do |image|
      name = image.split('/')[1]
      tags = extract_tags(name)

      { name: name, path: image, tags: tags }
    end

    renderer = ERB.new(@template, trim_mode: '-')
    content  = renderer.result(binding)

    File.write('Makefile', content)
  end

  private

  def extract_tags(name)
    tags = ['latest']

    if @values.dig('public', name, 'version', 'tag')
      tags.unshift(@values['public'][name]['version']['value'])
    elsif @values.dig('base', name, 'tag')
      tags.unshift(@values['base'][name]['value'])
    end

    tags
  end
end
