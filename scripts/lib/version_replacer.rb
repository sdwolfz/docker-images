# frozen_string_literal: true

class VersionReplacer
  def initialize(values)
    @values = values
  end

  def call
    @values['public'].each do |image, config|
      path = File.join('images', image, 'Dockerfile')
      next unless File.exist?(path)

      content = File.read(path)

      replace_base_image!(content, config)
      replace_package_version!(content, config)
      replace_package_sha!(content, config)

      File.write(path, content)
    end
  end

  private

  def replace_base_image!(content, config)
    base  = config['base']
    regex = Regexp.new(@values['base'][base]['regex'])
    value = @values['base'][base]['value']

    content.gsub!(regex, "\\1#{value}")
  end

  def replace_package_version!(content, config)
    return unless config['version']

    regex = config['version']['regex']
    value = config['version']['value']

    content.gsub!(Regexp.new(regex), "\\1#{value}\\2")
  end

  def replace_package_sha!(content, config)
    return unless config['sha']

    regex = config['sha']['regex']
    value = config['sha']['value']

    content.gsub!(Regexp.new(regex), "\\1#{value}\\2")
  end
end
