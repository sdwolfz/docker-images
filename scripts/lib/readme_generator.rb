# frozen_string_literal: true

require 'erb'

class ReadmeGenerator
  def initialize(values)
    @values = values
  end

  def call
    @template = File.read('templates/README.md.erb')

    renderer = ERB.new(@template, trim_mode: '-')
    content  = renderer.result(binding)

    File.write('README.md', content)
  end
end
