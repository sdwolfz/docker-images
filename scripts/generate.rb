#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

values = YAML.safe_load(File.read(File.join('values', 'images.yml')))

# ------------------------------------------------------------------------------
# Versions
# ------------------------------------------------------------------------------

require_relative './lib/version_replacer'

VersionReplacer.new(values).call

# ------------------------------------------------------------------------------
# Templates
# ------------------------------------------------------------------------------

require_relative './lib/makefile_generator'
require_relative './lib/readme_generator'

MakefileGenerator.new(values).call
ReadmeGenerator.new(values).call
